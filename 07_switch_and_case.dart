void main() {

  // Switch Case Statements: Applicable for ONLY 'int' and 'String', never bool

  String grade = 'A';

  switch (grade) {

    case 'A':
      print("Excellent grade of A");
      break; //GET OUT OF THE LOOP

    case 'B':
      print("Very Good !");
      break;

    case 'C':
      print("Good enough. But work hard");
      break;

    case 'F':
      print("You have failed");
      break;
    default:
      print("Invalid Grade");
  }
}