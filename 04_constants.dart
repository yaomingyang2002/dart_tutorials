void main() {
  //if never want to change a value then use final and const keywords

  // final, only be set once and it is initialized when accessed
  final cityName = 'Montreal';
  //	cityName = 'Peter';     // Throws an error, it cannot be changed

  final String countryName = 'Canada';

  // const, is implicitly final but it is a compile-time constant -> it initialized only during compilation
  const PI = 3.14;
  const double gravity = 9.8;
}

class Circle {
  // instance variable can be final but cannot be const

  final color = 'Red';

  //if you want a Constant at Class level then make it static const!
  static const PI = 3.14;
  // const PI = 3.14; error
}